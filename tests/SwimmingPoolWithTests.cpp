#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest.h>
#include <SwimmingPool.h>

TEST_CASE("Add Water")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	int time = 120;
	int flowRateIn = 1;
	pool.addWater(flowRateIn, time);
	CHECK(pool.getAmountOfWaterInPool() == time * flowRateIn);
}

TEST_CASE("Add Water - Overflow")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	int time = pool.getPoolTotalCapacity() + 1;
	int flowRateIn = 1;
	CHECK_THROWS_AS(pool.handleWaterOperation(flowRateIn, time, SwimmingPool::OperationType::DrainWater), std::logic_error);
}

TEST_CASE("Drain Water")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	pool.amountOfWaterInPool = 120;
	int time = 120;
	int flowRateOut = 1;
	pool.drainWater(flowRateOut, time);
	CHECK(pool.getAmountOfWaterInPool() == 0);
}

TEST_CASE("Drain Water - Underflow")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	pool.amountOfWaterInPool = 120;
	int time = pool.amountOfWaterInPool + 1;
	int flowRateOut = 1;
	CHECK_THROWS_AS(pool.handleWaterOperation(flowRateOut, time, SwimmingPool::OperationType::DrainWater), std::logic_error);
}

TEST_CASE("Get Amount Needed to fill")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	CHECK(pool.getAmountNeededToFill() == pool.getPoolTotalCapacity());

	int time = 5;
	int flowRateIn = 1;
	pool.addWater(flowRateIn, time);
	CHECK_FALSE(pool.getAmountNeededToFill() == pool.getPoolTotalCapacity());
	CHECK(pool.getAmountNeededToFill() == 30.0 * 15.0 * 10.0 * 7.48 - 5);
}

TEST_CASE("Pool Capacity")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	CHECK(pool.getPoolTotalCapacity() == 30.0 * 15.0 * 10.0 * 7.48);
}

TEST_CASE("Time needed to fill the pool")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	CHECK(pool.getTimeToFillPool() == (30.0 * 15.0 * 10.0 * 7.48) / 20.0);
}

TEST_CASE("Time needed to drain the pool")
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	int flowRateIn = 20;
	int time = 120;
	double flowRateOut = 19.5;

	pool.addWater(flowRateIn, time);
	CHECK(pool.getTimeToDrainPool() == flowRateIn * time / flowRateOut);
}


void testProgram()
{
	SwimmingPool pool(30.0, 15.0, 10.0, 20.0, 19.5);
	pool.print();

	int timeToFillPool = static_cast<int>(pool.getTimeToFillPool());
	std::cout << "Water needed to fill pool: " << pool.getAmountNeededToFill() << " gallons\n"
		<< "Time needed to fill the pool:" << timeToFillPool / SwimmingPool::kHour << " hours " << timeToFillPool % SwimmingPool::kHour << " minutes\n";

	pool.addWater(pool.flowRateIn, timeToFillPool);
	int timeToDrainPool = static_cast<int>(pool.getTimeToDrainPool());
	std::cout << "After filling pool to capacity, amount of water in pool is: " << pool.amountOfWaterInPool << " gallons\n"
		<< "Time needed to drain the pool completely: " << timeToDrainPool / SwimmingPool::kHour << " hours " << timeToDrainPool % SwimmingPool::kHour << " minutes\n";

	pool.drainWater(pool.flowRateOut, pool.getTimeToDrainPool() / 2);

	timeToFillPool = static_cast<int>(pool.getTimeToFillPool());
	std::cout << "Amount of water left after draining half the pool: " << pool.amountOfWaterInPool << " gallons\n"
		<< "Time needed to fill the pool if it is half full: " << timeToFillPool / SwimmingPool::kHour << " hours " << timeToFillPool % SwimmingPool::kHour << " minutes\n";

	pool.addWater(pool.flowRateIn, 180);

	std::cout << "Amount of water after adding water an additional 3 hours: " << pool.amountOfWaterInPool << " gallons\n";

	pool.addWater(100, 50000);
}

int main() 
{
	doctest::Context context;

	context.setOption("no-breaks", true);

	int failedTestCounter = context.run(); 


	if (failedTestCounter == 0)
	{
	testProgram();
	}

	return failedTestCounter;
}