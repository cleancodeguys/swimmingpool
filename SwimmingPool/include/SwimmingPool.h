#pragma once
#include <cstdint>

struct SwimmingPool
{
public:
	enum class OperationType : std::uint8_t
	{
		AddWater,
		DrainWater,
	};

public:
	static constexpr int kHour = 60;
	static constexpr double kGallonsInCubic = 7.48;
	static constexpr int kDefaultAmountOfWater = 0;

public:
	SwimmingPool(double length, double width, double depth, double flowRateIn, double flowRateOut);

public:
	void print() const;

	double getAmountOfWaterInPool() const;

	double getPoolTotalCapacity() const;

	double getTimeToFillPool() const;

	double getTimeToDrainPool() const;

	double getAmountNeededToFill() const;

	void addWater(double flowRate, double time);

	void drainWater(double flowRate, double time);

	void handleWaterOperation(double flowRate, double time, OperationType operationType);
private:
	double setPrecisionToOne(double);
public:
	double length;
	double width;
	double depth;
	double amountOfWaterInPool;
	double flowRateIn;
	double flowRateOut;
};

