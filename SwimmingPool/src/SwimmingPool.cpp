#include "SwimmingPool.h"
#include <iostream>
#include <stdexcept>
#include <cassert>
#include <cmath>

SwimmingPool::SwimmingPool(double length, double width, double depth, double flowRateIn, double flowRateOut)
	: length(length),
	width(width),
	depth(depth),
	flowRateIn(flowRateIn),
	flowRateOut(flowRateOut),
	amountOfWaterInPool(kDefaultAmountOfWater)
{}

void SwimmingPool::print() const
{
	std::cout << "Pool data:\n"
		<< "Length: " << length << " feet\n"
		<< "Width: " << width << " feet\n"
		<< "Depth: " << depth << " feet\n"
		<< "Amount of water in pool: " << amountOfWaterInPool << " gallons\n"
		<< "Fill rate: " << flowRateIn << " gal/min\n"
		<< "Drain rate: " << flowRateOut << "gal/min\n";
}

double SwimmingPool::getAmountOfWaterInPool() const
{
	return amountOfWaterInPool;
}

double SwimmingPool::getPoolTotalCapacity() const
{
	return length * depth * width * kGallonsInCubic;
}

double SwimmingPool::getTimeToFillPool() const
{
	return getAmountNeededToFill() / flowRateIn;
}

double SwimmingPool::getTimeToDrainPool() const
{
	return amountOfWaterInPool / flowRateOut;
}

double SwimmingPool::getAmountNeededToFill() const
{
	return getPoolTotalCapacity() - amountOfWaterInPool;
}


void SwimmingPool::addWater(double flowRate, double time)
{
	try
	{
		handleWaterOperation(flowRate, time, SwimmingPool::OperationType::AddWater);
	}
	catch (const std::logic_error& e)
	{
		std::cerr << e.what() << '\n';
	}
}

void SwimmingPool::drainWater(double flowRate, double time)
{
	try
	{
		handleWaterOperation(flowRate, time, SwimmingPool::OperationType::DrainWater);
	}
	catch (const std::logic_error& e)
	{
		std::cerr << e.what() << '\n';
	}

}

void SwimmingPool::handleWaterOperation(double flowRate, double time, SwimmingPool::OperationType operationType)
{	
	//double << std::setprecision(2);
	const double totalCapacity = getPoolTotalCapacity();
	switch (operationType)
	{
	case SwimmingPool::OperationType::AddWater:
	{
		time = setPrecisionToOne(time);

		amountOfWaterInPool += flowRate * time;
		if (amountOfWaterInPool > totalCapacity)
		{
			amountOfWaterInPool = totalCapacity;
			throw std::logic_error("Pool overflow.");
		}
		break;
	}
	case SwimmingPool::OperationType::DrainWater:
	{	
		time = setPrecisionToOne(time);

		amountOfWaterInPool -= flowRate * time ;
		if (amountOfWaterInPool < kDefaultAmountOfWater)
		{
			amountOfWaterInPool = kDefaultAmountOfWater;
			throw std::logic_error("Pool underflow.");
		}
		break;
	}
	default:
		throw std::invalid_argument("Unsupported operation.");
	}
}

double SwimmingPool::setPrecisionToOne(double time)
{
	time = static_cast<int>(time*10);
	return time / 10;
}
